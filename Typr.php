<?php

namespace azbuco\typr;

use yii\base\InvalidConfigException;
use yii\helpers\BaseInflector;
use yii\helpers\Json;
use yii\widgets\InputWidget;

abstract class Typr extends InputWidget
{
    /**
     * @var string Url to the data source for the suggestions
     */
    public $serviceUrl;
    
    /**
     * @var array JS script parameters
     */
    public $clientOptions = [];
    
    /**
     * @var array JS events for the script.
     */
    public $clientEvents = [];

    /**
     * @var array the HTML attributes for the typr input tag.
     */
    public $options = [];
    
    public $inputTemplate = '{input}';

    /**
     * Return the HTML representation for the typr component
     * Input for autocomplete,
     * Select for tags, select and multiselect
     */
    abstract public function renderInput();

    /**
     * return the type of the script (autocomplete, tags, select, multiselect)
     */
    abstract public function type();

    /**
     * Initializes the widget.
     *
     * @throws InvalidConfigException if the "serviceUrl" property is not set.
     */
    public function init()
    {
        parent::init();
        if (empty($this->serviceUrl)) {
            throw new InvalidConfigException("The 'serviceUrl' property must be set.");
        }

        $this->clientOptions['serviceUrl'] = $this->serviceUrl;
        $this->clientOptions['type'] = $this->type();
    }

    /**
     * @Inheritdoc
     */
    public function run()
    {
        $this->renderInput();
        $this->registerClientScript();
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $js = '';
        $view = $this->getView();
        $id = $this->options['id'];
        $var = 'typr_' . BaseInflector::id2camel($id);
        $options = Json::htmlEncode($this->clientOptions);

        $js = '; var ' . $var . ' = Typr("#' . $id . '", ' . $options . ');';

        $events = '';
        foreach ($this->clientEvents as $name => $event) {
            $events .= '$("#' . $id . '").on("' . $name . '", ' . $event . ');';
        }

        TyprAsset::register($view);
        $view->registerJs($js);
        $view->registerJs($events);
    }
}
