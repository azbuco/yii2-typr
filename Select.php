<?php

namespace azbuco\typr;

use Closure;
use yii\helpers\Html;

class Select extends Typr
{
    public $text;
    public $textAttribute;

    public function type()
    {
        return 'select';
    }

    public function renderInput()
    {
        if ($this->hasModel()) {
            $items = [];
            $value = Html::getAttributeValue($this->model, $this->attribute);
            if ($this->text instanceof Closure) {
                $text = call_user_func($this->text, $value, $this->model, $this->attribute);
                if ($text) {
                    $items[$value] = $text;
                }
            } else if ($this->textAttribute) {
                $tagAttribute = $this->textAttribute;
                if ($tagAttribute) {
                    $items[$value] = $this->model->$tagAttribute;
                }
            } else {
                $items[$value] = $value;
            }
            echo Html::activeDropDownList($this->model, $this->attribute, $items, $this->options);
        } else {
            $items = [];
            if ($this->value) {
                if ($this->text instanceof Closure) {
                    $text = call_user_func($this->text, $this->value);
                    if ($text) {
                        $items[$this->value] = $text;
                    }
                } else {
                    $items[$this->value] = $this->value;
                }
            }
            echo Html::dropDownList($this->name, $this->value, $items, $this->options);
        }
    }
}
