<?php

namespace azbuco\typr;

use Closure;
use yii\helpers\Html;

class Tags extends Typr
{
    public $text;
    public $textAttribute;

    public function type()
    {
        return 'tags';
    }

    public function renderInput()
    {
        $this->options['multiple'] = 'multiple';

        if ($this->hasModel()) {
            $items = [];
            $value = Html::getAttributeValue($this->model, $this->attribute);
            if ($this->text instanceof Closure) {
                $text = call_user_func($this->text, $value, $this->model, $this->attribute);
                if ($text) {
                    $items[$value] = $text;
                }
            } else if ($this->textAttribute) {
                $tagAttribute = $this->textAttribute;
                if ($tagAttribute) {
                    $items[$value] = $this->model->$tagAttribute;
                }
            } else {
                $items[$value] = $value;
            }
            echo Html::activeDropDownList($this->model, $this->attribute, $items, $this->options);
        } else {
            $items = [];
            if ($this->value) {
                if ($this->text instanceof Closure) {
                    foreach ($this->value as $value) {
                        $text = call_user_func($this->text, $value);
                        if ($text) {
                            $items[$value] = $text;
                        }
                    }
                } else {
                    foreach ($this->value as $value) {
                        $items[$value] = $value;
                    }
                }
            }
            echo Html::dropDownList($this->name, $this->value, $items, $this->options);
        }
    }
}
