<?php

namespace azbuco\typr;

use yii\web\AssetBundle;

class TyprAsset extends AssetBundle
{
    public $sourcePath = '@azbuco/typr/assets';
    public $css = [
        'css/typr.css',
    ];
    public $js = [
        'js/autogrow-input.js',
        'js/position.js',
        'js/typr.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
