var Typr = function (el, options) {

    var api = {};

    var k = api.keys = {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        ESC: 27,
        SPACE: 32,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        END: 35,
        HOME: 36,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        DELETE: 46
    };

    var t = api.types = {
        AUTOCOMPLETE: 'autocomplete',
        TAGS: 'tags',
        SELECT: 'select',
        MULTISELECT: 'multiselect'
    };

    api.el = el instanceof jQuery ? el : $(el);

    // it typr already initialized for the component
    if (api.el.data('typr')) {
        return api.el.data('typr');
    }

    api.defaults = {
        type: 'autocomplete', // autocomplete, tags, select, multiselect
        serviceUrl: null,
        queryVar: 'q',
        params: {},
        class: '',
        debugMode: false,
        position: {
            my: 'left top',
            at: 'left bottom+5',
            collision: 'flip flip',
            remove: false
        },
        placeholder: '',
        ignoreKeys: [
            k.DOWN, k.LEFT, k.RIGHT, k.UP, k.ENTER, k.TAB, k.HOME, k.END, k.PAGE_DOWN, k.PAGE_UP, k.ESC, k.SHIFT, k.ALT, k.CTRL
        ],
        getKey: function (item, index, data, params) {
            var key = Object.keys(item)[0];
            return item[key];
        },
        getValue: function (item, index, data, params) {
            var key = Object.keys(item)[0];
            return item[key];
        },
        renderSuggestion: function (item, index) {
            return api.renderDefaultSuggestion(item, index);
        },
        renderSuggestions: function (el, data, rawData) {
            return api.renderSuggestions(el, data, rawData);
        },
        renderTag: function (tag, key, value) {
            return tag;
        },
        hiddenInput: function (api) {
            var name = api.settings.multiple ? api.name + '[]' : api.name;
            return $('<input type="hidden" />')
                    .attr('name', name);
        },
        parse: function (rawData, key) {
            if (typeof key === 'undefined' || key === 'data') {
                return rawData['data'] ? rawData['data'] : rawData;
            }

            return rawData[key] ? rawData[key] : [];
        }
    };

    var s = api.settings = $.extend({}, api.defaults, options);

    api.name = api.el.attr('name');

    api.wrapper = $('<div class="typr">').addClass(s.class).addClass('form-control');

    api.tags = $('<span class="typr-tags">');

    api.input = $('<input type="text">').addClass("typr-input").attr('autocomplete', 'off'); //$('<input type="text" class="typr-input" />');

    api.suggestions = $('<div class="typr-suggestions" style="display: none;">');

    api.type = null;

    var multi = null;
    var select = null;

    api.load = function () {
        if (typeof api.jqxhr !== 'undefined') {
            api.jqxhr.abort();
        }
         
        // show suggestions dropdown 
        api.open();

        var search = api.input.val();
        
        // store search expression
        api.el.attr('data-query', search);
        
        // perform search
        api.settings.params[s.queryVar] = search;
        api.jqxhr = $.getJSON(s.serviceUrl, s.params)
                .done(function (data, textStatus, jqXHR) {
                    api.triggerEvent('load', {
                        data: data,
                        textStatus: textStatus,
                        jqXHR: jqXHR
                    });
                    api.render(data);
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    if (textStatus !== 'abort' && s.debugMode === true) {
                        console.log('Typr XHR error', textStatus);
                    }
                });
    };

    api.triggerEvent = function (eventType, extraParams) {
        var event = $.Event(eventType + '.typr', extraParams);
        if (s.debugMode) {
            console.log('Typr event', event);
        }
        return api.el.trigger(event);
    };

    api.render = function (rawData) {
        api.suggestions.empty();

        var defaults = api.renderSuggestions(rawData, 'defaults');
        var data = api.renderSuggestions(rawData);

        var items = [];
        items = api.input.val() === '' ? items.concat(defaults) : items;
        if (items) {
            items.push(api.renderSeparator());
        }
        items = items.concat(data);
        items = items.concat(api.renderTotal(data, rawData));
        items = items.concat(api.renderEmpty(data));

        api.suggestions.append(items);
        api.positionDropdown();

        api.triggerEvent('render', {
            data: rawData,
            items: items
        });
    };

    api.renderDefaultSuggestion = function (item, index) {
        return s.getValue(item, index);
    };

    api.renderSuggestion = function (item, index) {
        return $('<div class="typr-suggestion">')
                .attr('data-key', s.getKey(item, index))
                .attr('data-value', s.getValue(item, index))
                .data('suggestion', item)
                .append(s.renderSuggestion(item, index));
    };

    api.renderSuggestions = function (rawData, type) {
        var type = typeof type === 'undefined' ? 'data' : type;

        var suggestions = [];

        var data = s.parse(rawData, type);
        if (data) {
            $.each(data, function (index, item) {
                suggestions.push(api.renderSuggestion(item, index));
            });
        }

        return suggestions;
    };

    api.renderTotal = function (data, rawData) {
        if (rawData.total) {
            var moreCnt = rawData.total - data.length;
            if (moreCnt > 0) {
                return $('<div class="typr-more">Még további ' + moreCnt + ' elem</div>');
            }
        }
        return false;
    };

    api.renderEmpty = function (data) {
        if (data.length === 0) {
            return $('<div class="typr-more">Nincs találat</div>');
        }
        return false;
    };

    api.renderSeparator = function () {
        return $('<div class="typr-separator"></div>');
    };

    api.renderTags = function () {
        if (s.type === t.TAGS || s.type === t.SELECT || s.type === t.MULTISELECT) {
            api.wrapper.find('.typr-tag').remove();
            $.each(api.el.find('option'), function () {
                var option = $(this);
                api.wrapper.prepend(api.renderTag(option.attr('value'), option.text()));
            });
        }

        api.triggerEvent('renderTags');
    };

    api.renderTag = function (key, value) {
        var tag = $('<span class="typr-tag">')
                .attr('data-key', key)
                .html(value)
                .append($('<span class="typr-tag-close">'));

        return s.renderTag(tag, key, value);
    };

    api.open = function () {
        if (api.suggestions.is(':visible')) {
            return;
        }

        $(api).triggerHandler('open');
        api.suggestions.show(function () {
            api.triggerEvent('opened');
            api.positionDropdown();
        });
    };

    api.close = function () {
        api.triggerEvent('close');
        api.suggestions.fadeOut(200, function () {
            api.suggestions.empty();
            api.triggerEvent('closed');
        });
    };

    api.isOpen = function () {
        return api.suggestions.is(':visible');
    };

    api.positionDropdown = function () {
        var position = $.extend({}, api.settings.position, {
            of: api.wrapper
        });
        api.suggestions.position(position);
    };

    api.clear = function () {
        if (api.triggerEvent('beforeClear') !== false) {
            api.input.val('');
            api.tags.empty();
            api.triggerEvent('cleared');
        }
    };

    api.prev = function () {
        var selected = api.wrapper.find('.typr-selected');
        api.wrapper.find('.typr-suggestion').removeClass('typr-selected');
        if (selected.length > 0) {
            var prev = selected.prevAll('.typr-suggestion:first');
            prev.addClass('typr-selected');
        } else {
            api.wrapper.find('.typr-suggestion:last').addClass('typr-selected');
        }
    };

    api.next = function () {
        var selected = api.wrapper.find('.typr-selected');
        api.wrapper.find('.typr-suggestion').removeClass('typr-selected');
        if (selected.length > 0) {
            var next = selected.nextAll('.typr-suggestion:first');
            next.addClass('typr-selected');
        } else {
            api.wrapper.find('.typr-suggestion:first').addClass('typr-selected');
        }
    };

    api.mark = function (key) {
        api.suggestions.find('.typr-suggestion').removeClass('typr-selected');

        if (key instanceof jQuery) {
            key.addClass('typr-selected');
        }

        if (typeof key === 'string') {
            api.suggestions.filter(function () {
                return this.data('key') === key;
            }).addClass('typr-selected');
        }
    };

    api.applySelect = function () {
        var selected = api.getSelected();
        if (selected) {
            var key = selected.data('key');
            var value = selected.data('value');
            var suggestion = selected.data('suggestion');
            api.addValue(key, value);

            api.triggerEvent('pick', {
                key: key,
                value: value,
                source: selected,
                suggestion: suggestion
            });
        }
        api.close();

    };

    api.getSelected = function () {
        var selected = api.suggestions.find('.typr-selected');
        return selected.length > 0 ? selected : null;
    };

    api.getSuggestionCount = function () {
        return api.suggestions.find('.typr-suggestion').length;
    };

    api.getFirstSuggestion = function () {
        return api.suggestions.find('.typr-suggestion').filter(':first');
    };

    api.getValue = function () {
        return api.el.val();
    };

    api.getValueAsObject = function () {
        if (s.type === t.AUTOCOMPLETE) {
            return api.el.val();
        }

        var v = {};
        $.each(api.el.find('option:selected'), function () {
            var t = $(this);
            v[t.attr('value')] = t.html();
        });

        return v;
    };

    api.setValue = function (o) {
        if (s.type === t.AUTOCOMPLETE) {
            api.el.val(o);
            api.refresh();
            return;
        }

        var old = api.getValue();

        api.el.find('option').remove();

        $.each(o, function (index, item) {
            var value = '';
            var data = {};
            if (typeof item === 'object') {
                value = o.value;
                data = o.data;
            } else {
                value = item;
            }
            
            var option = $('<option>')
                    .attr('value', index)
                    .attr('selected', 'selected')
                    .attr('data-typr', JSON.stringify(data))
                    .text(value).change();

            api.el.append(option);
        });

        api.triggerEvent('change', {
            oldValue: old,
            newValue: api.getValue()
        });

        api.el.trigger('change', {
            oldValue: old,
            newValue: api.getValue()
        });

        api.refresh();
    };

    api.addValue = function (key, value, data) {
        var old = api.getValue();

        if (s.type === t.AUTOCOMPLETE) {
            api.el.val(key)
            api.input.val(key);
        } else {
            if (typeof value === 'undefined') {
                value = key;
            }
            if (typeof data === 'undefined') {
                data = {};
            }

            var option = $('<option>')
                    .attr('value', key)
                    .attr('selected', 'selected')
                    .attr('data-typr', JSON.stringify(data))
                    .text(value).change();

            api.el.append(option);

            api.input.val('');
        }

        api.triggerEvent('beforeAdd', {
            key: key,
            value: value,
            data: data,
            oldValue: old
        });

        api.refresh();
    };

    api.hasValue = function () {
        var value = api.getValue();

        return (value) ? true : false;
    };

    api.removeValue = function (key) {
        if (s.type === t.AUTOCOMPLETE) {
            return;
        }

        var result = api.triggerEvent('beforeRemove');

        if (result !== false) {
            api.el.find('option').filter(function () {
                return $(this).attr('value') == key;
            }).remove();

            api.triggerEvent('removed');

            api.refresh();
        }
    };

    api.removeLastValue = function () {
        if (s.type === t.AUTOCOMPLETE) {
            api.setValue('');
        }

        api.wrapper.find('option').last().remove();

        api.triggerEvent('removed');

        api.refresh();
    };

    api.refresh = function () {
        if (s.type === t.AUTOCOMPLETE) {
            api.input.val(api.el.val());
        } else {
            api.renderTags();
        }

        api.disable(api.el[0].hasAttribute('disabled'));
        api.readonly(api.el[0].hasAttribute('readonly'));

        api.input.trigger('update');

        api.wrapper.toggleClass('typr-has-value', api.hasValue());
        api.positionDropdown();

        api.triggerEvent('refresh');
    };
    
    api.disable = function (isDisable) {
        if (isDisable) {
            api.input.attr('disabled', 'disabled');
            api.el.attr('disabled', 'disabled');
            api.wrapper.addClass('disabled');
        } else {
            api.input.removeAttr('disabled');
            api.el.removeAttr('disabled');
            api.wrapper.removeClass('disabled');
        }
    };

    api.readonly = function (isReadonly) {
         if (isReadonly) {
            api.input.attr('readonly', 'readonly');
            api.el.attr('readonly', 'readonly');
            api.wrapper.addClass('readonly');
        } else {
            api.input.removeAttr('readonly');
            api.el.removeAttr('readonly');
            api.wrapper.removeClass('readonly');
        }
    };

    var mouseEvents = function () {

        // click anywhere outside dropdown close it
        $(document).click(function (e) {
            if (api.suggestions.is(':visible')) {
                var t = $(e.target);

                // if click is inside do nothing
                if (t.closest(api.wrapper).length > 0) {
                    return;
                }

                // if not close and block the possible accidental events
                e.preventDefault();
                e.stopPropagation();
                api.close();
            }
        });

        api.wrapper.on('click', '.typr-tag-close', function (e) {
            api.removeValue($(this).closest('.typr-tag').data('key'));
        });

        api.wrapper.click(function () {
            api.input.focus();
        });

        api.suggestions.click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            var suggestion = $(e.target).closest('.typr-suggestion');
            api.mark(suggestion);
            api.input.focus();
            api.applySelect();
        });


    };

    var keyboardEvents = function () {
        // press esc to close
        $(document).keyup(function (e) {
            if (api.suggestions.is(':visible')) {
                var keyCode = e.which;
                if (keyCode === k.ESC) {
                    api.close();
                }
            }
        });

        // input events
        api.input.keydown(function (e) {
            var keyCode = e.which;
            var value = api.input.val();

            if (keyCode === k.DELETE || keyCode === k.BACKSPACE) {
                if ((s.type === t.TAGS || s.type === t.SELECT || s.type === t.MULTISELECT) && api.hasValue() && value === '') {
                    api.removeLastValue();
                }
                return;
            }

            if (keyCode === k.DOWN) {
                e.preventDefault();
                e.stopPropagation();

                if (!api.isOpen()) {
                    api.load();
                    return;
                }

                api.next();
                return;
            }

            if (keyCode === k.UP) {
                e.preventDefault();
                e.stopPropagation();
                api.prev();
                return;
            }
        });

        api.input.keypress(function (e) {
            var keyCode = e.which;
            var value = api.input.val();

            switch (keyCode) {
                case k.ENTER:
                    if (api.isOpen() && api.getSelected()) {
                        e.preventDefault();
                        e.stopPropagation();
                        api.applySelect();
                        // ha van kiválasztott elem, az applyselect lekezel mindent
                        break;
                    }

                    if (!select) {
                        api.addValue(value, value);
                        api.close();
                    }
//                    Enable firs selection on enter. It may be ambigious...
//                    if (select && api.isOpen() && api.getSelected() === null) {
//                        var first = api.getFirstSuggestion();
//                        if (first) {
//                            api.mark(first)
//                            e.preventDefault();
//                            e.stopPropagation();
//                            api.applySelect();
//                            break;
//                        }
//                    }

                    if (select || multi) {
                        api.input.val('');
                    }

                    break;

                case k.DOWN:
                    if (!value) {
                        api.load();
                    }
            }
        });

        // minden olyan esemény, ahol az inputba már be van íródva a leütött billentyű
        api.input.keyup(function (e) {
            var keyCode = e.which;
            var value = api.input.val();

            if (s.type === t.SELECT && api.hasValue()) {
                api.input.val('');
                return;
            }

            if (s.ignoreKeys.indexOf(keyCode) === -1 && value !== '') {
                api.load();
            }

            if (s.type === t.AUTOCOMPLETE && s.ignoreKeys.indexOf(keyCode) === -1) {
                api.el.val(value);
            }

            api.positionDropdown();
        });
    };

    var inputEvents = function () {
        api.input.blur(function (e) {
            api.close();

            var value = api.input.val();

            // not selected partial input is added as tag on leave
            if (value !== '' && s.type === t.TAGS) {
                api.addValue(value, value);
                api.input.val('');
            }

            // clear input if partial content is not accepted
            if (s.type === t.SELECT || s.type === t.MULTISELECT) {
                api.input.val('');
            }

//            api.wrapper.find('[data-type="typr-hidden"]').remove();
//
//
//            $.each(api.tags.find('.typr-tag'), function () {
//                var tag = $(this);
//
//                var name = api.el.data('typr-name');
//                if (s.type === t.TAGS || s.type === t.MULTISELECT) {
//                    name = name + '[]';
//                }
//
//                var input = $('<input type="hidden" data-type="typr-hidden">')
//                        .attr('name', name)
//                        .attr('value', tag.data('key'));
//
//                api.wrapper.append(input);
//
//
//            });
        });

        api.input.focusin(function () {
            api.wrapper.addClass('typr-in-focus');
        });

        api.input.focusout(function () {
            api.wrapper.removeClass('typr-in-focus');
        });

    };

    var init = function () {
        api.el.addClass('typr-el');
        api.el.wrap(api.wrapper);
        api.el.data('typr', api);
        api.el.attr('tabindex', '-1');
        api.wrapper = api.el.parent();
        api.wrapper.append(api.placeholder);
        api.wrapper.append(api.tags);
        api.wrapper.append(api.input);
        api.wrapper.append(api.suggestions);

        api.wrapper.addClass('typr-' + s.type);
        api.wrapper.addClass(api.el.data('typr-class'));

        api.input.autoGrowInput({minWidth: 15, comfortZone: 15});

        api.refresh();

        multi = (s.type === t.TAGS || s.type === t.MULTISELECT) ? true : false;
        select = (s.type === t.SELECT || s.type === t.MULTISELECT) ? true : false;

        mouseEvents();
        keyboardEvents();
        inputEvents();
        
        api.setValue(api.getValueAsObject());
    };

    init();

    return api;
};