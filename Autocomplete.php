<?php

namespace azbuco\typr;

use yii\helpers\Html;

class Autocomplete extends Typr
{

    public function type()
    {
        return 'autocomplete';
    }

    public function renderInput()
    {
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }
    }
}
